# Findhotel

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

You can access the production build of this application on [findhotel-assessment.netlify.com](https://findhotel-assessment.netlify.com/)

## How do things work? (for interview)
- There is a function that is responsible for generating hotel, or a list of hotels with random data. This function gets called each time that the application is loaded, and all functions work against these generated data 
  ([here](https://gitlab.com/98kermani/findhotel/-/blob/master/src/app/shared/service/data-generator/data-generator.service.ts))
- When the application starts, 100 random hotels are generated and used in the application (created by the function explained above)  
- Data Models exist on [app/models](https://gitlab.com/98kermani/findhotel/-/tree/master/src/app/models) directory
- Filters and sorting data are stored in the URL for the following reasons: 1- To allow users to navigate to the previous filter or sort. 2- To make the page sharable, it would be helpful for campaigns, or when users want to send a link to a friend, or even store a search page.
- There is a service, and pipe which detect current device type (mobile or desktop), and it used to show suitable filter component based on the device type ([here](https://gitlab.com/98kermani/findhotel/-/blob/master/src/app/search-filter/search-filter.component.html))
- The mobile & desktop detection is done by a technique which can be used in server-side rendering too, and it's good for adaptive websites, I used the same technique with some more tricks to having two different versions of mobile and desktop but with almost the same core in the footballi.net, and you can test it by mobile and desktop to see the quality and flexibility for each version  


## Which dependencies used and why
Using some of the dependencies was not necessary, but I just wanted to make everything works more standard, like how we might work on a big project, and I also wanted to give you a better sense of my skill, knowledge, and experiences. The following dependencies used in the project, and I explain why in it’s less known:
* **Bootstrap 4**
* **Normalize.css**
* **Include-media:** Simple, elegant and maintainable media queries in Sass
* **mobile-detect.js:** Device detection (phone, tablet, desktop, mobile grade, os, versions). This package used to adaptively show the suitable component for the device. ([mobile-detect.js](https://github.com/hgoebl/mobile-detect.js))
* **lorem-ipsum**: To create lorem-ipsum text



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
