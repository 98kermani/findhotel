import {Component, OnInit} from '@angular/core';
import {DataGeneratorService} from '../shared/service/data-generator/data-generator.service';
import {Hotel} from '../models/hotel.model';
import {ActivatedRoute} from '@angular/router';
import {HotelService} from '../hotel/hotel-service/hotel.service';

@Component({
  selector: 'app-search-result-page',
  templateUrl: './search-result-page.component.html',
  styleUrls: ['./search-result-page.component.scss']
})
export class SearchResultPageComponent implements OnInit {
  hotels: Hotel[];
  allHotels: Hotel[];




  constructor(private dataGeneratorService: DataGeneratorService,
              private activatedRoute: ActivatedRoute,
              private hotelService: HotelService) {
    this.allHotels = this.dataGeneratorService.generateListOfHotels(100);
    this.hotels = this.allHotels;
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.hotels = this.allHotels;
      let stars = [];
      let rate;
      let distance;
      let price;
      // get different filters and store them to the variables defined above
      if (params.hasOwnProperty('filterStar') && params.filterStar) {
        // split selected stars and convert it to number
        stars = params.filterStar.split(',').map(x => +x);
      }


      if (params.hasOwnProperty('filterRate') && params.filterRate) {
        rate = Number(params.filterRate);
      }

      if (params.hasOwnProperty('filterDistance') && params.filterDistance) {
        distance = Number(params.filterDistance);
      }

      if (params.hasOwnProperty('filterPrice') && params.filterPrice) {
        price = Number(params.filterPrice);
      }


      if (params.hasOwnProperty('greatDeal') && params.greatDeal) {
        this.hotels = this.hotelService.getGreatDealHotels(this.hotels);
      }

      if (rate || stars || distance || price) {
        this.hotels = this.hotelService.filterHotels(this.hotels, stars, rate, distance, price);
      }

      // Get the sort type, and sort the list of hotels by the given type
      if (params.hasOwnProperty('sort') && params.sort) {
        this.hotels = this.hotelService.sortHotels(this.hotels, params.sort);
      }
    });
  }


}
