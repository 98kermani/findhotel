export interface UserRatingInput {
  title: string;
  value: number;
  active: boolean;
}
