export interface Hotel {
  id?: number;
  title: string;
  greatOffer: boolean;
  star: number;
  userRating: number;
  location: string;
  distance: number;
  image: string;
  reviewKeywords?: (string)[] | null;
  freeCancellation: boolean;
  sourceLinks: (ReservationSource)[] | null;
  description: string;
  favorite: boolean;
}

export interface ReservationSource {
  title: string;
  link: string;
  price: number;
  discountPercentage: number;
}
