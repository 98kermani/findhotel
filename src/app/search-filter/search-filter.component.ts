import {Component, Input, OnInit} from '@angular/core';
import {Hotel} from '../models/hotel.model';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit {

  @Input() hotels: Hotel[];

  constructor() {
  }

  ngOnInit(): void {
  }


}
