import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserRatingInput} from '../../models/user-rating-input.model';

@Component({
    selector: 'app-search-filter-mobile',
    templateUrl: './search-filter-mobile.component.html',
    styleUrls: ['./search-filter-mobile.component.scss']
})
export class SearchFilterMobileComponent implements OnInit {
    isFilterPage: boolean;
    guestRating: UserRatingInput[];
    starRating: UserRatingInput[];

    constructor(private activatedRoute: ActivatedRoute) {
        this.guestRating = [
            {title: 'any', value: 0, active: false},
            {title: '6+', value: 6, active: false},
            {title: '7+', value: 7, active: false},
            {title: '8+', value: 8, active: false},
            {title: '9+', value: 9, active: false},
        ];

        this.starRating = [
            {title: '1', value: 1, active: false},
            {title: '2', value: 2, active: false},
            {title: '3', value: 3, active: false},
            {title: '4', value: 4, active: false},
            {title: '5', value: 5, active: false},
        ];

    }

    setGuestRate(value: number) {
        this.guestRating = this.guestRating.map((item: UserRatingInput, index: number) => {
            item.active = item.value <= value;
            return item;
        });
    }

    getActiveGuestRate(guestRating: UserRatingInput[]) {
        let activeValue;
        guestRating.map((item) => {
            if (item.active) {
                activeValue = item.value;
            }
        });

        return activeValue;
    }

    activateStar(value: number) {
        this.starRating.map((star) => {
            if (star.value === value) {
                star.active = !star.active;
            }
            return star;
        });
    }

    getActiveStars(starRating: UserRatingInput[]): number[] | [] {
        const activeStars = [];
        starRating.map((item) => {
            if (item.active) {
                activeStars.push(item.value);
            }
        });
        return activeStars;
    }


    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            if (params.hasOwnProperty('filterModal')) {
                this.isFilterPage = params.filterModal === 'open';
            } else {
                this.isFilterPage = false;
            }

            if (params.hasOwnProperty('filterStar')) {
                const stars = params.filterStar.split(',');
                stars.map((star) => {
                    this.activateStar(Number(star));
                });
            }

            if (params.hasOwnProperty('filterRate')) {
                this.setGuestRate(Number(params.filterRate));
            }
        });
    }

}
