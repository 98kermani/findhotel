import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Hotel} from '../../models/hotel.model';
import {HotelService} from '../../hotel/hotel-service/hotel.service';

@Component({
  selector: 'app-search-filter-desktop',
  templateUrl: './search-filter-desktop.component.html',
  styleUrls: ['./search-filter-desktop.component.scss']
})
export class SearchFilterDesktopComponent implements OnInit {
  @Input() hotels: Hotel[];
  selectedRate = 0;
  selectedPrice = 1000;
  selectedDistance = 50;

  sortType;

  constructor(private router: Router,
              private hotelService: HotelService,
              private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.hasOwnProperty('filterRate')) {
        this.selectedRate = Number(params.filterRate);
      }

      if (params.hasOwnProperty('filterDistance')) {
        this.selectedDistance = Number(params.filterDistance);
      }

      if (params.hasOwnProperty('filterPrice')) {
        this.selectedPrice = Number(params.filterPrice);
      }
    });
  }

  formatKilometer(value: number) {
    return value + 'km';
  }

  changeSlider(change, type) {
    const value = change.value;
    if (type === 'filterPrice') {
      this.selectedPrice = value;
    } else if (type === 'filterRate') {
      this.selectedRate = value;
    } else if (type === 'filterDistance') {
      this.selectedDistance = value;
    }

    this.applyFilter();
  }

  getGreatDealHotels(hotels: Hotel[]): Hotel[] {
    return this.hotelService.getGreatDealHotels(hotels);
  }


  resortList() {
    if (this.sortType) {
      this.router.navigate(['/'],
        {
          queryParamsHandling: 'merge',
          queryParams: {
            sort: this.sortType
          }
        });
    }
  }

  applyFilter() {
    this.router.navigate(['/'],
      {
        queryParamsHandling: 'merge',
        queryParams: {
          filterRate: this.selectedRate,
          filterPrice: this.selectedPrice,
          filterDistance: this.selectedDistance
        }
      });
  }


}
