import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StarRatingComponent} from './shared/component/star-rating/star-rating.component';
import {HeaderComponent} from './header/header.component';
import {SearchResultPageComponent} from './search-result-page/search-result-page.component';
import {HotelComponent} from './hotel/hotel.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {UserRatingComponent} from './shared/component/user-rating/user-rating.component';
import {MatSliderModule} from '@angular/material/slider';
import {SearchFilterComponent} from './search-filter/search-filter.component';
import {IsMobilePipe} from './shared/pipe/isMobile/is-mobile.pipe';
import {SearchFilterMobileComponent} from './search-filter/search-filter-mobile/search-filter-mobile.component';
import {SearchFilterDesktopComponent} from './search-filter/search-filter-desktop/search-filter-desktop.component';
import {RouterModule} from '@angular/router';
import {CalculateDiscountPipe} from './shared/pipe/calculateDiscount/calculate-discount.pipe';
import {FormsModule} from '@angular/forms';
import { PriceHistogramComponent } from './price-histogram/price-histogram.component';

@NgModule({
  declarations: [
    AppComponent,
    StarRatingComponent,
    HeaderComponent,
    SearchResultPageComponent,
    HotelComponent,
    UserRatingComponent,
    SearchFilterComponent,
    IsMobilePipe,
    SearchFilterMobileComponent,
    SearchFilterDesktopComponent,
    CalculateDiscountPipe,
    PriceHistogramComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    MatIconModule,
    MatSliderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
