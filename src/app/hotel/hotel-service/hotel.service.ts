import {Injectable} from '@angular/core';
import {Hotel, ReservationSource} from '../../models/hotel.model';
import {HelperService} from '../../shared/service/helper/helper.service';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private helperService: HelperService) {
  }

  filterHotels(hotels: Hotel[],
               stars: number[],
               userRating: number,
               distance: number,
               price: number) {
    return hotels.filter((hotel: Hotel) => {
      if (!stars || stars.length === 0) {
        stars = [1, 2, 3, 4, 5];
      }

      if (!userRating) {
        userRating = 0;
      }

      if (!distance) {
        distance = 100;
      }

      if (!price) {
        price = 1000;
      }

      return !!(hotel.userRating >= userRating &&
        stars.indexOf(hotel.star) !== -1 &&
        hotel.distance <= distance &&
        this.helperService.calculateDiscount(hotel.sourceLinks[0].price, hotel.sourceLinks[0].discountPercentage) <= price
      );

    });
  }

  getGreatDealHotels(hotels: Hotel[]): Hotel[] {
    return hotels.filter((hotel: Hotel) => {
      if (hotel.greatOffer) {
        return hotel;
      }
      return false;
    });
  }

  sortHotels(hotels: Hotel[], type: string): Hotel[] {
    if (type === 'distance') {
      return hotels.sort((a, b) => {
        return a.distance - b.distance;
      });
    } else if (type === 'price') {
      return hotels.sort((a, b) => {
        const aHotelPrice = this.helperService.calculateDiscount(a.sourceLinks[0].price, a.sourceLinks[0].discountPercentage);
        const bHotelPrice = this.helperService.calculateDiscount(b.sourceLinks[0].price, b.sourceLinks[0].discountPercentage);
        return aHotelPrice - bHotelPrice;
      });
    } else if (type === 'rate') {
      return hotels.sort((a, b) => {
        return b.userRating - a.userRating;
      });
    } else {
      return hotels;
    }
  }



}
