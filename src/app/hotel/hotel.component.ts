import {Component, Input, OnInit} from '@angular/core';
import {Hotel} from '../models/hotel.model';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  @Input() hotel: Hotel;
  detailVisibility: boolean;

  constructor() {
  }


  ngOnInit(): void {
  }


  likeHotel() {
    this.hotel.favorite = !this.hotel.favorite;
  }

  showDetail() {
    this.detailVisibility = !this.detailVisibility;
  }

}
