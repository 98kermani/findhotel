import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-rating',
  templateUrl: './user-rating.component.html',
  styleUrls: ['./user-rating.component.scss']
})
export class UserRatingComponent implements OnInit {
  @Input() userRate: number;
  text: string;
  color: string;
  icon: string;

  constructor() {
  }

  ngOnInit(): void {
    if (this.userRate <= 3) {
      this.text = 'Not Good';
      this.color = '#ff6b50';
      this.icon = 'sentiment_very_dissatisfied';
    } else if (this.userRate <= 5) {
      this.text = 'Not Recommended';
      this.color = '#ffafaa';
      this.icon = 'sentiment_dissatisfied';
    } else if (this.userRate <= 7) {
      this.text = 'Good';
      this.color = '#b8ff85';
      this.icon = 'sentiment_satisfied';
    } else if (this.userRate <= 8) {
      this.text = 'Very Good';
      this.color = '#94ff58';
      this.icon = 'sentiment_very_satisfied';
    } else if (this.userRate <= 9) {
      this.text = 'Fabulous';
      this.color = '#5be825';
      this.icon = 'sentiment_very_satisfied';
    } else if (this.userRate <= 10) {
      this.text = 'Amazing';
      this.color = '#489b21';
      this.icon = 'sentiment_very_satisfied';
    }
  }

}
