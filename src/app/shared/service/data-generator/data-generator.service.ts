import {Injectable} from '@angular/core';
import {Hotel, ReservationSource} from '../../../models/hotel.model';
import {LoremIpsum} from 'lorem-ipsum';
import {HelperService} from '../helper/helper.service';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

@Injectable({
  providedIn: 'root'
})
export class DataGeneratorService {
  constructor(private helperService: HelperService) {
  }

  /**
   * Generate one random hotel
   */
  generateOneHotel(): Hotel {
    return {
      title: lorem.generateWords(this.helperService.rand(3, 8)),
      description: lorem.generateParagraphs(this.helperService.rand(2, 5)),
      freeCancellation: this.helperService.rand(0, 1) !== 0,
      greatOffer: this.helperService.rand(0, 1) !== 0,
      image: 'https://source.unsplash.com/500x350/?hotel,building,home',
      location: lorem.generateWords(this.helperService.rand(3, 8)),
      distance: this.helperService.rand(1, 50),
      star: this.helperService.rand(1, 5),
      userRating: this.helperService.rand(0, 10),
      reviewKeywords: new Array(this.helperService.rand(3, 8)).fill({}).map(() => {
        return lorem.generateWords(this.helperService.rand(1, 2));
      }),
      sourceLinks: new Array(this.helperService.rand(2, 3)).fill({}).map(() => {
        return this.getHotelReservationSource(this.helperService.rand(0, 6));
      }),
      favorite: false,
    };
  }

  generateListOfHotels(count: number): Hotel[] {
    return new Array(count).fill({}).map((item, index) => {
      const hotel = this.generateOneHotel();
      hotel.id = index;
      item = hotel;
      return item;
    });
  }

  getHotelReservationSource(index): ReservationSource {
    const reservationSystems = [];
    reservationSystems.push(
      {
        link: 'https://booking.com/reserve/hotel/112222xx',
        title: 'booking.com',
      });

    reservationSystems.push(
      {
        link: 'https://findhotel.net/reserve/hotel/112222xx',
        title: 'Findhotel.net',
      });

    reservationSystems.push(
      {
        link: 'https://trivago.com/reserve/hotel/112222xx',
        title: 'Trivago.com',
      });

    reservationSystems.push(
      {
        link: 'https://kayak.com/reserve/hotel/112222xx',
        title: 'Kayak.com',
      });

    reservationSystems.push(
      {
        link: 'https://expedia.nl/reserve/hotel/112222xx',
        title: 'expedia.nl'
      });

    reservationSystems.push(
      {
        link: 'https://7idea.com/reserve/hotel/112222xx',
        title: '7idea.com'
      });

    reservationSystems.push(
      {
        link: 'https://google.com/reserve/hotel/112222xx',
        title: 'Google Hotel'
      });

    reservationSystems[index].discountPercentage = this.helperService.rand(0, 60);
    reservationSystems[index].price = this.helperService.rand(5, 500);

    return reservationSystems[index];
  }
}
