import {Injectable} from '@angular/core';
import * as MobileDetect from 'mobile-detect';

@Injectable({
  providedIn: 'root'
})
export class MobileService {

  constructor() {
  }

  isMobile() {
    return !!(window.navigator.userAgent &&
      new MobileDetect(window.navigator.userAgent).mobile());
  }
}
