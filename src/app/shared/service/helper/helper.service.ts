import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() {
  }

  replaceAll(str: string, find: string, replace: string): string {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  thousandSeparator(value: number | string) {
    let newValue = value + '';
    if (newValue.indexOf(',') !== -1) {
      newValue = this.replaceAll(value + '', ',', '');
    }
    return newValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  rand(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  calculateDiscount(price, discount) {
    return Math.round(price - (price * discount / 100));
  }

}
