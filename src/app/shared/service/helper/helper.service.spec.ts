import {TestBed} from '@angular/core/testing';

import {HelperService} from './helper.service';

describe('HelperService', () => {
  let service: HelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should calculate the discount correctly', () => {
    expect(service.calculateDiscount(100, 10)).toBe(90);
    expect(service.calculateDiscount(500, 32)).toBe(340);
  });
});
