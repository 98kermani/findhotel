import { IsMobilePipe } from './is-mobile.pipe';
import {HelperService} from '../../service/helper/helper.service';
import {MobileService} from '../../service/mobile/mobile.service';

describe('IsMobilePipe', () => {
  it('create an instance', () => {
    const mobileService = new MobileService();
    const pipe = new IsMobilePipe(mobileService);
    expect(pipe).toBeTruthy();
  });
});
