import {Pipe, PipeTransform} from '@angular/core';
import {MobileService} from '../../service/mobile/mobile.service';

@Pipe({
  name: 'isMobile'
})
export class IsMobilePipe implements PipeTransform {

  constructor(private mobileService: MobileService) {

  }

  transform(value?: any, args?: any): any {
    return this.mobileService.isMobile();
  }
}
