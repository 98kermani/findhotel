import {Pipe, PipeTransform} from '@angular/core';
import {HelperService} from '../../service/helper/helper.service';

@Pipe({
  name: 'calculateDiscount'
})
export class CalculateDiscountPipe implements PipeTransform {

  constructor(private helperService: HelperService) {

  }

  transform(value: number | string, arg) {
    return this.helperService.calculateDiscount(value, arg);
  }

}
