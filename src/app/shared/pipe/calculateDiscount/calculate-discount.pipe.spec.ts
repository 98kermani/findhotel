import {CalculateDiscountPipe} from './calculate-discount.pipe';
import {HelperService} from '../../service/helper/helper.service';

describe('CalculateDiscountPipe', () => {
  it('create an instance', () => {
    const helperService = new HelperService();
    const pipe = new CalculateDiscountPipe(helperService);
    expect(pipe).toBeTruthy();
  });

  it('should calculate discount correctly', () => {
    const helperService = new HelperService();
    const pipe = new CalculateDiscountPipe(helperService);
    expect(pipe.transform(100, 10)).toBe(90);
  });
});
