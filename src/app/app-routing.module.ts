import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SearchResultPageComponent} from './search-result-page/search-result-page.component';

const routes: Routes = [
  {
    path: '',
    component: SearchResultPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
